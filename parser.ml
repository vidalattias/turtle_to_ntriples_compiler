open ASD
open Token
open Printf
open List

(*
let parse_object = parser
  [< 'STRING exp >] -> print_endline "Found a string !"
  |[< 'ENTITY exp >] -> print_endline "Found an entity !"

let rec parse_object_list = parser
  [< parse_object ; parse_object_list >] -> printf "greg"
  | [< parse_object >] -> printf "greg"

let parse_predicate = parser
  [< parse_object >] -> printf "trge"
  | [<>] -> print "frgre"

and predicate_list = parser
  [< parse_predicate ; 'SEMICOLON ; parse_predicate_list>] -> printf "First rule"
  | [< parse_predicate >] -> printf "tste"

and parse_triplet = parser
  [< 'ENTITY parse_predicate_list >] -> printf "First rule"

and parse_triplet_list = parser
  [< parse_triplet ; 'POINT ; parse_triplet_list>] -> printf "firestruel"
  | [<>] -> printf "First rule"

and parse = parser
  [< parse_triplet_list >] -> printf "First rule"
*)
let parse_object = parser
|[<'ENTITY str>] -> str
|[< 'STRING str>] -> str

let rec parse_object_list = parser
[<head = parse_object; tail=parse_object_list>] -> head::tail
|[<'COMMA; tail=parse_object_list>] -> tail
|[<>] -> []

let parse_predicate = parser
[< 'ENTITY pred; obj_list=parse_object_list >] -> predicate_construct pred obj_list

let rec parse_predicate_list = parser
[<head=parse_predicate; tail=parse_predicate_list>] -> head::tail
| [< 'SEMICOLON; tail=parse_predicate_list>] -> tail
|[<>] -> []

(*Returns a triplet*)
let parse_triplet = parser
[< 'ENTITY name; predicate_list=parse_predicate_list >] -> triplet_construct name predicate_list

(* Returns a triplet list *)
let rec parse_triplet_list = parser
[<head=parse_triplet;'POINT; tail=parse_triplet_list>] -> (head::tail)
| [< >] -> []

let parse = parser
[< triplet_list = parse_triplet_list >] -> triplet_list
