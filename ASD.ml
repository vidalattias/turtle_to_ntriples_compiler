(* ASD type *)
type predicate =
{
  predicate_id : string;
  objs : string list
}

and triplet =
{
  subject : string;
  predicates : predicate list
}

type text_turtle = Text_turtle of triplet list

let print_triplet () = print_endline "HTRHEH"


let rec predicate_construct name obj_list =
  let predicate_return = { predicate_id=name; objs=obj_list}
  in predicate_return

let rec triplet_construct name predicate_list =
  let triplet_return = { subject=name; predicates=predicate_list}
  in triplet_return

let rec triplet_list_to_turtle x = match x with
| [] -> print_endline "end"
| h::t -> print_endline "yeay"; triplet_list_to_turtle t

(* Function to generate the document out of the AST *)
(*let rec ntriples_of_ast = function*)
let rec obj_to_string subject predicate obj_list = match obj_list with
| [] -> ""
| h::[] -> (subject ^ " " ^ predicate ^ " " ^ h ^ " .")
| h::t -> (subject ^ " " ^ predicate ^ " " ^ h ^ " .") ^ "\n" ^ (obj_to_string subject predicate t)

let rec predicates_to_string subject pred_list = match pred_list with
| [] -> ""
| h::[] -> (obj_to_string subject h.predicate_id h.objs)
| h::t -> (obj_to_string subject h.predicate_id h.objs) ^ "\n" ^ (predicates_to_string subject t)

let rec ntriples_of_ast ast = match ast with
| [] -> ""
| h::[] -> (predicates_to_string h.subject h.predicates)
| h::t -> (predicates_to_string h.subject h.predicates) ^ "\n" ^ (ntriples_of_ast t)
